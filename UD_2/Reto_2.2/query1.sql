SELECT p.Name AS Playlist,
       t.Name,
       a.Title,
       t.UnitPrice
FROM Playlist p
JOIN PlaylistTrack pt 
ON p.PlaylistId = pt.PlaylistId
JOIN Track t 
ON pt.TrackId = t.TrackId
JOIN Album a 
ON t.AlbumId = a.AlbumId
WHERE p.PlaylistId IN (
    SELECT PlaylistId
    FROM Playlist
    WHERE Name LIKE 'M%'
    
)
ORDER BY a.Title, t.UnitPrice
LIMIT 3;
