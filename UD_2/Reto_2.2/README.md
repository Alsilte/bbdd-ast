# Reto 2.2: Consultas avanzadas

Alejandro Silla Tejero.

En este reto trabajamos con la base de datos `Chinook`, que nos viene dada en el siguiente repositorio: https://github.com/lerocha/chinook-database/. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados. Completado también con las observaciones del profersor Cristian en clase.

* [Enlace al repo UD_2](https://gitlab.com/Alsilte/bbdd-ast.git)


## Query 1


```sql
SELECT p.Name AS Playlist,
       t.Name,
       a.Title,
       t.UnitPrice
FROM Playlist p
JOIN PlaylistTrack pt 
ON p.PlaylistId = pt.PlaylistId
JOIN Track t 
ON pt.TrackId = t.TrackId
JOIN Album a 
ON t.AlbumId = a.AlbumId
WHERE p.PlaylistId IN (
    SELECT PlaylistId
    FROM Playlist
    WHERE Name LIKE 'M%'
    
)
ORDER BY a.Title, t.UnitPrice
LIMIT 3;
```

## Query 2

```sql
SELECT DISTINCT Artist.Name AS CANCIONES
FROM Artist
WHERE Artist.ArtistId IN (
  SELECT Album.ArtistId
  FROM Album
  JOIN Track ON Track.AlbumId = Album.AlbumId
  WHERE Track.Milliseconds > 300000
);

```

## Query 3

```sql
SELECT Employee.FirstName, Employee.LastName
FROM Employee
WHERE EXISTS (
    SELECT 1
    FROM Customer
    WHERE Customer.SupportRepId = Employee.EmployeeId
);

```

## Query 4

```sql
SELECT Track.Name AS "Canciones no compradas"
FROM Track
WHERE Track.TrackId NOT IN (
    SELECT TrackId
    FROM InvoiceLine
);
```

## Query 5


```sql
SELECT Manager.FirstName AS "Nombre empleado",
       Manager.LastName AS "Apellido empleado",
       Employee.FirstName AS "Nombre subordinado",
       Employee.LastName AS "Apellido Subordinado"
FROM Employee AS Employee
LEFT JOIN Employee AS Manager
   ON Manager.EmployeeId = Employee.ReportsTo
WHERE Manager.FirstName IS NOT NULL;
```

## Query 6

```sql
SELECT Track.Name AS Canción, Album.Title AS Album, Artist.Name AS Artista
FROM Invoice
JOIN InvoiceLine 
ON Invoice.InvoiceId = InvoiceLine.InvoiceId
JOIN Track 
ON InvoiceLine.TrackId = Track.TrackId
JOIN Album 
ON Track.AlbumId = Album.AlbumId
JOIN Artist 
ON Album.ArtistId = Artist.ArtistId
WHERE Invoice.CustomerId = (
    SELECT CustomerId
    FROM Customer
    WHERE Customer.FirstName = 'Luis' AND Customer.LastName = 'Rojas'
);
```

## Query 7

```sql
SELECT Track.Name AS Canción
FROM Track 
WHERE Track.UnitPrice > (
    SELECT MIN(UnitPrice)
    FROM Track
    WHERE Track.AlbumId = Track.AlbumId
);
```

## Query 8

```sql
SELECT Customer.FirstName, Customer.LastName
FROM Customer
WHERE Customer.CustomerId IN (
  SELECT DISTINCT CustomerId
  FROM Invoice
  WHERE InvoiceId IN (
    SELECT InvoiceId
    FROM InvoiceLine
    WHERE TrackId IN (  
      SELECT TrackId
      FROM Track
      WHERE AlbumId IN (
        SELECT AlbumId
        FROM Album
        WHERE ArtistId = (
          SELECT ArtistId
          FROM Artist
          WHERE Name = 'Aerosmith'
        )
      )
    )
  )
);
```

## Query 9

```sql
SELECT Customer.FirstName, Customer.LastName 
FROM Customer
WHERE (
    SELECT COUNT(*)
    FROM Invoice
    JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId
    WHERE Invoice.CustomerId = Customer.CustomerId
) > 20;
```

## Query 10

```sql
SELECT Track.Name AS Canción, COUNT(InvoiceLine.TrackId) AS Ventas
FROM Track
INNER JOIN InvoiceLine ON Track.TrackId = InvoiceLine.TrackId
GROUP BY Track.Name
ORDER BY Ventas DESC
LIMIT 10;
```

## Query 11

```sql
SELECT Name, Milliseconds AS Duración
FROM Track
WHERE Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
);
```

## Query 12

```sql
SELECT
  (SELECT 'Países con clientes') AS Paises,
  (SELECT COUNT(DISTINCT Country) 
  FROM Customer) AS Valor,
  (SELECT 'Número de géneros' ) AS Genero,
  (SELECT COUNT(DISTINCT GenreId) 
  FROM Genre) AS Valor,
  (SELECT 'Numero de canciones' ) AS Canciones,
  COUNT(*) AS Valor
FROM Track;
```

## Query 13

```sql
SELECT t.Name AS Canción, COUNT(il.InvoiceLineId) AS Ventas
FROM Track t
INNER JOIN InvoiceLine il ON t.TrackId = il.TrackId
GROUP BY t.TrackId
HAVING COUNT(il.InvoiceLineId) > (
    SELECT AVG(vpc)
    FROM (
        SELECT COUNT(InvoiceLineId) AS vpc
        FROM InvoiceLine
        GROUP BY TrackId
    ) AS MediaVentas
);
```

## Query 14

```sql
SELECT FirstName AS Nombre,
       LastName AS Apellido,
       Country AS País,
       CASE
           WHEN Country = 'USA' THEN 'Local'
           WHEN Country IS NULL THEN 'Unknown'
           ELSE 'International'
       END AS CustomerCategory
FROM Customer;
```

## Query 15

```sql
SELECT InvoiceId ,
       Total,
       CASE
           WHEN Total > 5 THEN Total * 0.1
           ELSE 0
       END AS Descuento
FROM Invoice;
```

## Query 16

```sql
SELECT FirstName AS Nombre,
       LastName AS Apellido,
       Title AS Puesto,
       CASE
           WHEN Title LIKE '%Manager%' THEN 'Manager'
           WHEN Title LIKE '%Assistant%' THEN 'Assistant'
           ELSE 'Employee'
       END AS Nivel
FROM Employee;
```

## Query 17

```sql
SELECT FirstName AS Nombre,
    LastName AS Apellido,
    TotalSpent AS Gasto,
    CASE
        WHEN TotalSpent > 45 THEN 'VIP'
        ELSE 'Normal'
    END AS "Tipo de cliente"
FROM (
    SELECT 
        c.CustomerId,
        c.FirstName,
        c.LastName,
        SUM(i.Total) AS TotalSpent
    FROM 
        Customer c
    INNER JOIN 
        Invoice i ON c.CustomerId = i.CustomerId
    GROUP BY 
        c.CustomerId, c.FirstName, c.LastName
) AS GastoCliente;
```


