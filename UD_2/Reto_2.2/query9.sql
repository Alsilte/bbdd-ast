SELECT Customer.FirstName, Customer.LastName 
FROM Customer
WHERE (
    SELECT COUNT(*)
    FROM Invoice
    JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId
    WHERE Invoice.CustomerId = Customer.CustomerId
) > 20;