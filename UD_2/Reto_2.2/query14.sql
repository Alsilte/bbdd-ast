SELECT FirstName AS Nombre,
       LastName AS Apellido,
       Country AS País,
       CASE
           WHEN Country = 'USA' THEN 'Local'
           WHEN Country IS NULL THEN 'Unknown'
           ELSE 'International'
       END AS CustomerCategory
FROM Customer;
