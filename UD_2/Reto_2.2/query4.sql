SELECT Track.Name AS "Canciones no compradas"
FROM Track
WHERE Track.TrackId NOT IN (
    SELECT TrackId
    FROM InvoiceLine
);
