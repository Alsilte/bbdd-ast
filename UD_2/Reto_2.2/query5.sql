SELECT Manager.FirstName AS "Nombre empleado",
       Manager.LastName AS "Apellido empleado",
       Employee.FirstName AS "Nombre subordinado",
       Employee.LastName AS "Apellido Subordinado"
FROM Employee AS Employee
LEFT JOIN Employee AS Manager
   ON Manager.EmployeeId = Employee.ReportsTo
WHERE Manager.FirstName IS NOT NULL;
