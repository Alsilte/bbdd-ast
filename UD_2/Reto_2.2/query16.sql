SELECT FirstName AS Nombre,
       LastName AS Apellido,
       Title AS Puesto,
       CASE
           WHEN Title LIKE '%Manager%' THEN 'Manager'
           WHEN Title LIKE '%Assistant%' THEN 'Assistant'
           ELSE 'Employee'
       END AS Nivel
FROM Employee;
