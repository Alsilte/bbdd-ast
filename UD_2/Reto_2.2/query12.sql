SELECT
  (SELECT 'Países con clientes') AS Paises,
  (SELECT COUNT(DISTINCT Country) 
  FROM Customer) AS Valor,
  (SELECT 'Número de géneros' ) AS Genero,
  (SELECT COUNT(DISTINCT GenreId) 
  FROM Genre) AS Valor,
  (SELECT 'Numero de canciones' ) AS Canciones,
  COUNT(*) AS Valor
FROM Track;
