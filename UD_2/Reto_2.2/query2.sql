SELECT DISTINCT Artist.Name AS CANCIONES
FROM Artist
WHERE Artist.ArtistId IN (
  SELECT Album.ArtistId
  FROM Album
  JOIN Track ON Track.AlbumId = Album.AlbumId
  WHERE Track.Milliseconds > 300000
);
