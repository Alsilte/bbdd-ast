SELECT Track.Name AS Canción, Album.Title AS Album, Artist.Name AS Artista
FROM Invoice
JOIN InvoiceLine 
ON Invoice.InvoiceId = InvoiceLine.InvoiceId
JOIN Track 
ON InvoiceLine.TrackId = Track.TrackId
JOIN Album 
ON Track.AlbumId = Album.AlbumId
JOIN Artist 
ON Album.ArtistId = Artist.ArtistId
WHERE Invoice.CustomerId = (
    SELECT CustomerId
    FROM Customer
    WHERE Customer.FirstName = 'Luis' AND Customer.LastName = 'Rojas'
);