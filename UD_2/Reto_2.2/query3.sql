SELECT Employee.FirstName, Employee.LastName
FROM Employee
WHERE EXISTS (
    SELECT 1
    FROM Customer
    WHERE Customer.SupportRepId = Employee.EmployeeId
);
