SELECT *
FROM usuarios 
JOIN reaccionesFotos
ON usuarios.idUsuario = reaccionesFotos.idUsuario
WHERE usuarios.idRol IN (
	SELECT roles.idRol FROM roles
	WHERE roles.descripcion = "Usuario"
    -- OR roles.descripcion = "Administrador"
)
AND idTipoReaccion = (
	SELECT idTipoReaccion FROM tiposReaccion
    WHERE tiposReaccion.descripcion = "Me gusta"
);