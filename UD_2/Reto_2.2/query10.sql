SELECT Track.Name AS Canción, COUNT(InvoiceLine.TrackId) AS Ventas
FROM Track
INNER JOIN InvoiceLine ON Track.TrackId = InvoiceLine.TrackId
GROUP BY Track.Name
ORDER BY Ventas DESC
LIMIT 10;
