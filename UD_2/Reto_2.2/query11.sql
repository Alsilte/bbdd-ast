SELECT Name, Milliseconds AS Duración
FROM Track
WHERE Milliseconds > (
    SELECT AVG(Milliseconds)
    FROM Track
);