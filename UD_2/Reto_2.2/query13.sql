SELECT t.Name AS Canción, COUNT(il.InvoiceLineId) AS Ventas
FROM Track t
INNER JOIN InvoiceLine il ON t.TrackId = il.TrackId
GROUP BY t.TrackId
HAVING COUNT(il.InvoiceLineId) > (
    SELECT AVG(vpc)
    FROM (
        SELECT COUNT(InvoiceLineId) AS vpc
        FROM InvoiceLine
        GROUP BY TrackId
    ) AS MediaVentas
);
