SELECT FirstName AS Nombre,
    LastName AS Apellido,
    TotalSpent AS Gasto,
    CASE
        WHEN TotalSpent > 45 THEN 'VIP'
        ELSE 'Normal'
    END AS "Tipo de cliente"
FROM (
    SELECT 
        c.CustomerId,
        c.FirstName,
        c.LastName,
        SUM(i.Total) AS TotalSpent
    FROM 
        Customer c
    INNER JOIN 
        Invoice i ON c.CustomerId = i.CustomerId
    GROUP BY 
        c.CustomerId, c.FirstName, c.LastName
) AS GastoCliente;
