SELECT Track.Name AS Canción
FROM Track 
WHERE Track.UnitPrice > (
    SELECT MIN(UnitPrice)
    FROM Track
    WHERE Track.AlbumId = Track.AlbumId
);
