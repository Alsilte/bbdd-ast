SELECT Customer.FirstName, Customer.LastName
FROM Customer
WHERE Customer.CustomerId IN (
  SELECT DISTINCT CustomerId
  FROM Invoice
  WHERE InvoiceId IN (
    SELECT InvoiceId
    FROM InvoiceLine
    WHERE TrackId IN (  
      SELECT TrackId
      FROM Track
      WHERE AlbumId IN (
        SELECT AlbumId
        FROM Album
        WHERE ArtistId = (
          SELECT ArtistId
          FROM Artist
          WHERE Name = 'Aerosmith'
        )
      )
    )
  )
);
