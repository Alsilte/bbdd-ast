SELECT g.Name, COUNT(*) as 'Número de canciones'
FROM Track AS t
JOIN Genre AS g
WHERE t.GenreId = g.GenreId
GROUP BY g.Name
ORDER BY COUNT(*) DESC;