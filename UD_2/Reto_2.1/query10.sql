SELECT l.Name AS "Lista de reproducción", t.Name AS "Canción", a.Title AS "Álbum", t.Milliseconds AS "Duración"
FROM Playlist AS l
JOIN PlaylistTrack AS p
JOIN Track AS t
JOIN Album AS a
ON l.PlaylistId = p.PlaylistId
AND p.TrackId = t.TrackId
AND t.AlbumId = a.AlbumId
WHERE l.Name LIKE "C%"
ORDER BY t.AlbumId, t.Milliseconds Desc;

