-- Muestra los nombres de los 5 empleados más jóvenes junto a los nombres de
-- sus supervisores, si los tienen
SELECT e.FirstName AS 'Empleado',
s.FirstName AS 'Supervisor'
FROM Employee AS e
JOIN Employee AS s
WHERE e.EmployeeId = s.ReportsTo
ORDER BY e.BirthDate DESC
LIMIT 5;

