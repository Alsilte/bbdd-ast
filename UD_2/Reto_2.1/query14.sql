SELECT COUNT(TrackId) AS 'Número de canciones'
FROM Track AS t
JOIN Album AS a
WHERE t.AlbumId = a.AlbumId
AND a.Title = 'Out Of Time';