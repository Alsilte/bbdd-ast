-- Muestra las 10 canciones que más tamaño ocupan.
SELECT Name AS 'Canciones',
Bytes AS 'Tamaño' 
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
