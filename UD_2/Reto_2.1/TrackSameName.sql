-- Canciones que tienen el mismo nombre
USE Chinook;
SELECT COUNT(Name) - COUNT(DISTINCT Name)
FROM Track;