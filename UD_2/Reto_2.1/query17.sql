SELECT a.Title AS 'Álbum',
COUNT(*) AS 'Número de canciones'
FROM Track AS t
JOIN Album AS a
ON t.AlbumId = a.AlbumId
GROUP BY a.AlbumId
ORDER BY COUNT(*) DESC;

-- query para comprobar que ese albúm tiene las canciones 
-- SELECT *
-- FROM Track
-- WHERE AlbumId = 141;