SELECT c.FirstName AS NOMBRE,
c.LastName AS Apellido,
p.Total
FROM Customer AS c
JOIN Invoice AS p
WHERE p.CustomerId = c.CustomerId
AND p.Total > 10
ORDER BY c.LastName ASC;