-- Muestra todas las canciones compuestas por AC/DC
SELECT Name AS 'Canción',
Composer AS 'Cantante'
FROM Track
WHERE Composer LIKE 'AC/DC';