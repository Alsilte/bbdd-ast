-- Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: 
-- fecha de la factura, nombre completo del cliente, dirección de facturación,
-- código postal, país, importe (en este orden).
SELECT f.InvoiceDate AS 'Fecha Factura',
c.FirstName AS 'Nombre',
c.LastName AS 'Apellido',
f.BillingAddress AS 'Dirección de facturación',
f.BillingPostalCode AS 'Código postal',
f.BillingCountry AS 'País',
f.Total AS 'Importe'
FROM Invoice AS f
JOIN Customer AS c
WHERE f.CustomerId = c.CustomerId
AND f.BillingCity LIKE 'Berlin';