# Reto 2.1: Consultas avanzadas

Alejandro Silla Tejero.

En este reto trabajamos con la base de datos `Chinook`, que nos viene dada en el siguiente repositorio: https://github.com/lerocha/chinook-database/. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados. Completado también con las observaciones del profersor Cristian en clase.

* [Enlace al repo UD_2](https://gitlab.com/Alsilte/bbdd-ast.git)


## Query 1
Esta consulta devuelve el nombre y apellido de todos los clientes de Francia. La consulta selecciona las columnas `FirstName` y `LastName` de la tabla Customer, utilizamos `WHERE` para filtrar los resultados en función de la columna Country, que debe ser igual a 'France'. La palabra clave `AS` se utiliza para especificar los alias de las columnas seleccionadas, que se muestran  como "Nombre" y "Apellido" en el resultado.

```sql
-- Encuentra todos los clientes de Francia.
SELECT 
FirstName As Nombre,
LastName As Apellido
FROM Chinook.Customer
WHERE Country = 'France';
```

## Query 2
Esta consulta devuelve las facturas cuyas fechas estén en el primer trimestre del año actual. La función `YEAR(CURRENT_DATE)` devuelve el año actual, y `MONTH(InvoiceDate) BETWEEN 1 AND 3` filtra las facturas entre enero y marzo de cualquier año.

```sql
SELECT InvoiceId AS 'Número',
InvoiceDate AS 'Fecha factura'
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(CURRENT_DATE)
AND MONTH(InvoiceDate) BETWEEN 1 AND 3;
```

## Query 3
Esta consulta devuelve la canción y el cantante de todas las canciones de AC/DC. La consulta selecciona las columnas `Name` y `Composer` de la tabla `Track`, utilizamos `WHERE` para filtrar los resultados en función de la columna Composer, que debe ser igual `LIKE` a AC/DC.

```sql
-- Muestra todas las canciones compuestas por AC/DC
SELECT Name AS 'Canción',
Composer AS 'Cantante'
FROM Track
WHERE Composer LIKE 'AC/DC';
```

## Query 4
Esta consulta devuelve las canciones y el tamaño de la 10 que más ocupan. La consulta selecciona las columnas `Name` y `Bytes` de la tabla `Track`, ordenadas por bytes de forma descendente `ORDER BY DESC` y usamos `Limit` para que solo aparezcan las 10 primeras.

```sql
-- Muestra las 10 canciones que más tamaño ocupan.
SELECT Name AS 'Canciones',
Bytes AS 'Tamaño' 
FROM Track
ORDER BY Bytes DESC
LIMIT 10;
```

## Query 5
Esta consulta devuelve los nombres de los países en los que tenemos clientes. La consulta selecciona las columnas `Country`de la tabla `Customer` utilizamos el `Distinct` para que solo aparezca una vez cada país.

```sql
-- Muestra el nombre de aquellos países en los que tenemos clientes.
SELECT DISTINCT Country AS 'Paises'
FROM Customer;
```

## Query 6
Para seleccionar todos los géneros, selecionamos `Name` de la tabla `Genre`y con un `DISTINCT` para que no se repita el género. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
-- Muestra todos los géneros musicales
SELECT DISTINCT Name AS 'Género'
FROM Genre
```
## Query 7
Esta consulta devuelve los nombres de los artistas y los títulos de sus álbumes. La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Album` y `Artist`, basándose en la columna `ArtistId` que existe en ambas tablas. La cláusula `SELECT` especifica las columnas `Name` de la tabla `Artist` y `Title`de la tabla Album para mostrar en el resultado.
`
```sql
-- Muestra todos los artistas junto a sus álbumes.
SELECT c.Name,
a.Title
FROM Album AS a
JOIN Artist AS c
WHERE c.ArtistId = a.ArtistId
```

## Query 8
Esta consulta devuelve los nombres de los 5 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen. iLa consulta utliza una cláusula `JOIN` para combinar las filas de la tabla `Employee` consigo misma, basándose en la columna `ReportsTo` que existe en la tabla. La cláusula `SELECT` especifica las columnas `FirstName` de la tabla `Employee` para mostrar el nombre de los empleados y de sus supervisores. La cláusula `WHERE` filtra los resultados en función de la columna `EmployeeId`, que debe ser igual al `ReportsTo` del mismo registro. La cláusula `ORDER BY` ordena los resultados por la columna `BirthDate` de los empleados en orden descendente, y se limita a los 5 primeros con LIMIT 5.

```sql
-- Muestra los nombres de los 5 empleados más jóvenes junto a los nombres de
-- sus supervisores, si los tienen
SELECT e.FirstName AS 'Empleado',
s.FirstName AS 'Supervisor'
FROM Employee AS e
JOIN Employee AS s
WHERE e.EmployeeId = s.ReportsTo
ORDER BY e.BirthDate DESC
LIMIT 5;
```
## Query 9
Esta consulta devuelve todas las facturas de los clientes berlineses, mostrando las columnas `InvoiceDate`, `FirstName`, `LastName`, `BillingAddress`, `BillingPostalCode`, `BillingCountry`, y `Total`. La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Invoice` y `Customer`, basándose en la columna `CustomerId` que existe en ambas tablas. La cláusula `WHERE` filtra los resultados en función de la columna `BillingCity`, que debe ser igual a 'Berlin'.

```sql
SELECT f.InvoiceDate AS 'Fecha Factura',
c.FirstName AS 'Nombre',
c.LastName AS 'Apellido',
f.BillingAddress AS 'Dirección de facturación',
f.BillingPostalCode AS 'Código postal',
f.BillingCountry AS 'País',
f.Total AS 'Importe'
FROM Invoice AS f
JOIN Customer AS c
WHERE f.CustomerId = c.CustomerId
AND f.BillingCity LIKE 'Berlin';
```

## Query 10
Esta consulta devuelve las listas de reproducción, canciones, álbumes y duraciones de las canciones que comienzan por la letra "C". La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Playlist`, `PlaylistTrack`, `Track`, y `Album`, basándose en las columnas `PlaylistId`, `TrackId`, y `AlbumId` que existen en las tablas correspondientes. La cláusula `WHERE` filtra los resultados en función de la columna `Name` de la tabla `Playlist`, que debe empezar por la letra "C". La cláusula `ORDER BY` ordena los resultados por la columna `AlbumId` y la columna `Milliseconds` de la tabla `Track` en orden descendente.

```sql
SELECT l.Name AS "Lista de reproducción", t.Name AS "Canción", a.Title AS "Álbum", t.Milliseconds AS "Duración"
FROM Playlist AS l
JOIN PlaylistTrack AS p
JOIN Track AS t
JOIN Album AS a
ON l.PlaylistId = p.PlaylistId
AND p.TrackId = t.TrackId
AND t.AlbumId = a.AlbumId
WHERE l.Name LIKE "C%"
ORDER BY t.AlbumId, t.Milliseconds Desc;
```

## Query 11
Esta consulta devuelve los nombres y apellidos de los clientes junto al total de sus facturas, si el total de sus facturas es mayor a 10. La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Customer` y `Invoice`, basándose en la columna `CustomerId` que existe en ambas tablas. La cláusula `WHERE` filtra los resultados en función de la columna `Total` de la tabla `Invoice`, que debe ser mayor a 10.

```sql
SELECT c.FirstName AS NOMBRE,
c.LastName AS Apellido,
p.Total
FROM Customer AS c
JOIN Invoice AS p
WHERE p.CustomerId = c.CustomerId
AND p.Total > 10
ORDER BY c.LastName ASC;
```

## Query 12
Esta consulta devuelve el importe medio, el importe mínimo, y el importe máximo de las facturas. La consulta utiliza las funciones `AVG`, `MIN`, y `MAX` para calcular el importe medio, el importe mínimo, y el importe máximo respectivamente.

```sql
SELECT 
AVG(Total) AS 'Importe medio', 
MIN(Total) AS 'Importe minimo', 
MAX(Total) AS 'Importe maximo'
FROM Invoice;
```

## Query 13
Esta consulta devuelve el número total de artistas. La consulta utiliza la función `COUNT` para contar el número total de filas de la tabla `Artist`.

```sql
SELECT COUNT(*) 
FROM Artist;
```

## Query 14
Esta consulta devuelve el número total de canciones del álbum "Out Of Time". La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Track` y `Album`, basándose en la columna `AlbumId` que existe en ambas tablas. La cláusula `WHERE` filtra los resultados en función de la columna `Title` de la tabla `Album`, que debe ser igual a "Out Of Time". La función `COUNT` cuenta el número total de filas de la tabla `Track` que cumplen la condición.

```sql
SELECT COUNT(TrackId) AS 'Número de canciones'
FROM Track AS t
JOIN Album AS a
WHERE t.AlbumId = a.AlbumId
AND a.Title = 'Out Of Time';
```

## Query 15
Esta consulta devuelve el número total de países diferentes de los clientes. La consulta utiliza la función `COUNT` junto con la cláusula `DISTINCT` para contar el número total de países diferentes de la columna `Country` de la tabla `Customer`.

```sql
SELECT COUNT(DISTINCT Country) AS "Número de países"
FROM Customer;
```

## Query 16
Esta consulta devuelve el número total de canciones por género, ordenadas por el número total de canciones en orden descendente. La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Track` y `Genre`, basándose en la columna `GenreId` que existe en ambas tablas. La cláusula `GROUP BY` agrupa los resultados por el nombre del género.

```sql
SELECT g.Name, COUNT(*) as 'Número de canciones'
FROM Track AS t
JOIN Genre AS g
WHERE t.GenreId = g.GenreId
GROUP BY g.Name
ORDER BY COUNT(*) DESC;
```

## Query 17
Esta consulta devuelve el título de los álbumes junto al número total de canciones de cada álbum. La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Track` y `Album`, basándose en la columna AlbumId que existe en ambas tablas. La cláusula `GROUP BY` agrupa los resultados por el título del álbum. La cláusula `ORDER BY` ordena los resultados por el número total de canciones en orden descendente.

```sql
SELECT a.Title AS 'Álbum',
COUNT(*) AS 'Número de canciones'
FROM Track AS t
JOIN Album AS a
ON t.AlbumId = a.AlbumId
GROUP BY a.AlbumId
ORDER BY COUNT(*) DESC;

-- query para comprobar que ese albúm tiene las canciones 
-- SELECT *
-- FROM Track
-- WHERE AlbumId = 141;
```

## Query 18
Esta consulta devuelve el nombre de los géneros junto al número total de ventas de cada género. La consulta utiliza una cláusula `JOIN` para combinar las filas de las tablas `Genre`, `Track`, `InvoiceLine`, y `Invoice`, basándose en las columnas `GenreId`, `TrackId`, `InvoiceId`, y `TrackId` que existen en las tablas correspondientes. La cláusula `GROUP BY` agrupa los resultados por el nombre del género. La cláusula `ORDER BY` ordena los resultados por el número total de ventas en orden descendente.

```sql
SELECT g.Name AS Genre, 
COUNT(g.GenreId) AS 'Número de ventas'
FROM Genre g
JOIN Track t 
ON g.GenreId = t.GenreId
JOIN InvoiceLine 
ON t.TrackId = InvoiceLine.TrackId
JOIN Invoice 
ON InvoiceLine.InvoiceId = Invoice.InvoiceId
GROUP BY g.Name
ORDER BY COUNT(g.GenreId) DESC;
```

## Query 19
Esta consulta recupera el título del álbum y el número total de compras de cada álbum. Utiliza la instrucción JOIN para combinar las tablas `Album`, `Track` y `InvoiceLine` en función de sus respectivas claves foráneas. La función `SUM` se utiliza para calcular el número total de compras de cada álbum, y la cláusula `GROUP BY` agrupa los resultados por el título del álbum. Por último, la cláusula `ORDER BY` ordena los resultados en orden descendente en función del número total de compras y la cláusula `LIMIT` limita los resultados a los 6 primeros álbumes con más compras.

```sql
-- Agrupar por Id ya que un albúm puede ser igual pero de diferentes cantantes
SELECT 
a.Title AS 'Álbumn', 
COUNT(*) AS 'Compras'
FROM Album AS a
JOIN Track AS t
ON a.AlbumId = t.AlbumId
JOIN InvoiceLine AS il
ON t.TrackId = il.TrackId
GROUP BY a.AlbumId
ORDER BY COUNT(*) DESC
LIMIT 6;
```

## Query 20
Esta consulta devuelve el país junto al número total de clientes de cada país, siempre y cuando el número total de clientes sea mayor o igual a 5. La consulta utiliza la función `COUNT` junto con la cláusula `DISTINCT` para contar el número total de clientes diferentes de cada país. La cláusula `GROUP BY` agrupa los resultados por el país de facturación. La cláusula `HAVING` filtra los resultados en función del número total de clientes diferentes de cada país.

```sql
SELECT i.BillingCountry AS Country,
COUNT(DISTINCT i.CustomerId) AS 'Número de clientes'
FROM Invoice AS i
GROUP BY i.BillingCountry
HAVING COUNT(DISTINCT i.CustomerId) >= 5;
```