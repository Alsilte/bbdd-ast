SELECT g.Name AS Genre, 
COUNT(g.GenreId) AS 'Número de ventas'
FROM Genre g
JOIN Track t 
ON g.GenreId = t.GenreId
JOIN InvoiceLine 
ON t.TrackId = InvoiceLine.TrackId
GROUP BY g.Name
ORDER BY COUNT(g.GenreId) DESC;