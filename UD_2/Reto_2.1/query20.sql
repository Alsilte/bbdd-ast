SELECT i.BillingCountry AS Country,
COUNT(DISTINCT i.CustomerId) AS 'Número de clientes'
FROM Invoice AS i
GROUP BY i.BillingCountry
HAVING COUNT(DISTINCT i.CustomerId) >= 5;