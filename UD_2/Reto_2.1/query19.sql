SELECT 
a.Title AS 'Álbumn', 
COUNT(*) AS 'Compras'
FROM Album AS a
JOIN Track AS t
ON a.AlbumId = t.AlbumId
JOIN InvoiceLine AS il
ON t.TrackId = il.TrackId
GROUP BY a.AlbumId
ORDER BY COUNT(*) DESC
LIMIT 6;