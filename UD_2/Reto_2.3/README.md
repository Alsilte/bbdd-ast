# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

* [Enlace al repo](https://gitlab.com/Alsilte/bbdd-ast.git)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.

En esta consulta mostramos el `Name` y `Milliseconds` de la tabla `Track` donde `Milliseconds` sea mayor y dentro del `WHERE` metemos la subconsulta, donde de la tabla `Track` sacamos la media de tiempo con `AVG(Milliseconds)`.

```sql
SELECT Name AS Canción,
Milliseconds AS Duración
FROM Track
WHERE Milliseconds > (SELECT AVG(Milliseconds)
					FROM Track);
```

### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

Esta consulta selecciona todas las facturas de la tabla `Invoice` donde el `CustomerId` corresponde al ID del cliente cuyo correo electrónico es "emma_jones@hotmail.com" que obtenemos como subconsulta dentro del `WHERE` . Luego, ordena los resultados por `InvoiceId` en orden descendente y limita los resultados a 5 `LIMIT`.
```sql
SELECT * 
FROM Invoice
WHERE CustomerId = 
		(SELECT CustomerId 
        FROM Customer 
        WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceId DESC
LIMIT 5;

```

## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

Esta consulta selecciona todas las listas de reproducción de la tabla `Playlist` donde el `PlaylistId` está presente en la subconsulta. La subconsulta selecciona todos los `PlaylistId` de la tabla `PlaylistTrack` donde el `TrackId` está presente en otra subconsulta. La segunda subconsulta selecciona todos los `TrackId` de la tabla `Track` donde el `GenreId` corresponde al ID del género cuyo nombre es "Reggae".

```sql
SELECT * 
FROM Playlist
WHERE PlaylistId IN (
	SELECT PlaylistId 
  FROM PlaylistTrack
	WHERE TrackId IN (
		SELECT TrackId 
    FROM Track
		WHERE GenreId = (
			SELECT GenreId 
      FROM Genre
			WHERE Name = "Reggae"
      )
		)
	);
```

### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

Esta consulta selecciona el ID del cliente `CustomerId`, el nombre `FirstName` y el apellido `LastName` de la tabla `Customer` donde el `CustomerId` está presente en una subconsulta. La subconsulta selecciona todos los `CustomerId` de la tabla `Invoice` donde el `Total` de la factura es mayor que 20.

```sql
SELECT CustomerId,
FirstName AS Nombre,
LastName AS Apellido 
FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId 
  FROM Invoice
	WHERE Total > 20);
```

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.

Esta consulta selecciona el ID del álbum `AlbumId`, el título `Title` del álbum y el nombre del artista `Name` de la tabla `Album` y `Artist`, respectivamente, donde el ID del artista en la tabla `Album` coincide con el ID del artista en la tabla `Artist`. En la subconsulta, se cuentan el número de pistas por álbum (`AlbumId`) en la tabla `Track`. Solo los álbumes que tienen más de 15 pistas son seleccionados. Finalmente, los resultados se ordenan por `AlbumId`.

```sql
SELECT a.AlbumId, 
a.Title, 
ar.Name 
FROM Album a
JOIN Artist ar 
ON a.ArtistId = ar.ArtistId
WHERE a.AlbumId IN (
	SELECT AlbumId FROM Track
	GROUP BY AlbumId
	HAVING COUNT(*) > 15)
ORDER BY a.AlbumId;
```

### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.

Esta consulta selecciona todos los registros de la tabla `Album` donde el `AlbumId` está presente en una subconsulta. La subconsulta calcula el promedio del número de canciones por álbum. Luego, compara el número de canciones de cada álbum con este promedio y selecciona los álbumes que tienen más canciones que el promedio. `HAVING` se utiliza en la subconsulta filtrar sobre el resultado de la función de agregación AVG(N_Canciones), para seleccionar los álbumes cuyo número de canciones es mayor que el promedio.

```sql
SELECT * 
FROM Album
WHERE AlbumId IN (
	SELECT AlbumId
	FROM Track
	GROUP BY AlbumId
	HAVING COUNT(*) > (
		SELECT AVG(N_Canciones) 
    FROM(
			SELECT AlbumId,
      Count(*) AS N_Canciones
			FROM Track
			GROUP BY AlbumId
			) 
		AS Album_NCanciones));
```
### Consulta 7
Obtener los álbumes con una duración total superior a la media.

Esta consulta selecciona todos los registros de la tabla `Album` donde el `AlbumId` 
está presente en una subconsulta. La subconsulta calcula la suma total de la duración en milisegundos de todas 
las pistas por álbum. Luego, se calcula el promedio de esta duración total de pistas por álbum.
`HAVING` se utiliza en la subconsulta  para obtener los resultados de la suma total de duración en milisegundos de cada álbum, para que solo se seleccionen los álbumes cuya duración total de pistas sea mayor que el promedio.

```sql
SELECT * 
FROM Album
WHERE AlbumId IN (
	SELECT AlbumId AS T_dur
	FROM Track
	GROUP BY AlbumId
	HAVING SUM(Milliseconds) > (
		SELECT AVG(T_dur) 
    FROM(
			SELECT AlbumId,
      SUM(Milliseconds) AS T_dur
			FROM Track
			GROUP BY AlbumId
			) 
		AS Album_TDur));
```

### Consulta 8
Canciones del género con más canciones.

Tenemos una subconsulta dentro de la cláusula `JOIN` que relaciona los resultados de la subconsulta con la tabal `Track`.  Esta subconsulta selecciona todos los `GenreId` de la tabla `Genre`. Luego, para cada `GenreId`, cuenta el número de canciones en la tabla `Track` que tienen el mismo `GenreId`. El resultado es una lista  GenreId y su recuento de canciones correspondiente, ordenada en orden descendente. `LIMIT` limita los resultados de la subconsulta interna a solo el primer resultado.

```sql
SELECT *
FROM Track t
JOIN (
	SELECT g.GenreId
  FROM Genre g
  ORDER BY(
		SELECT COUNT(*) 
    FROM Track
    WHERE GenreId = g.GenreId
    ) DESC
	LIMIT 1 
  ) AS sub
ON t.GenreId = sub.GenreId
```

### Consulta 9
Canciones de la _playlist_ con más canciones.

Esta subconsulta selecciona todos las `PlaylistID` de la tabla `Playlist`. La subconsulta cuenta el número de canciones en cada playlist donde se cuenta el número de `TrackId` en la tabla `PlaylistTrack` que coincide con el `PlaylistId` de la playlist actual. `LIMIT 1` selecciona solo la primera fila. Faltaría sacar todas las canciones de esa playlist.
```sql
-- SELECT *
-- FROM Track t
-- JOIN (
	SELECT PlaylistId,
	(	SELECT COUNT(TrackId) 
		FROM PlaylistTrack pt
		WHERE pt.PlaylistId = p.PlaylistId
		)AS CuentaCanciones
	FROM Playlist p
	ORDER BY CuentaCanciones DESC
	LIMIT 1;
--    ) AS sub
-- ON t.TrackId = sub.TrackId; 

```


## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

Esta consulta selecciona el `CustomerId`, `FirstName` y `LastName` de cada cliente de la tabla `Customer`. Luego, utiliza una subconsulta para calcular el total gastado por cada cliente en todas sus compras. La subconsulta calcula la suma total de la columna `Total` de la tabla `Invoice` para cada cliente, donde el `CustomerId` de la tabla `Invoice` coincide con el `CustomerId` de la tabla `Customer` en la fila actual. El resultado de esta subconsulta se asigna a la columna `TotalGastado`. 

```sql
SELECT 
c.CustomerId,
c.FirstName,
c.LastName AS CustomerName,
    (SELECT SUM(i.Total)
     FROM Invoice i
     WHERE i.CustomerId = c.CustomerId
    ) AS TotalGastado
FROM 
    Customer c;
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

Esta consulta selecciona el `EmployeeId`, `FirstName`, y `LastName` de la tabla `Employee`. Luego, utiliza una subconsulta para contar el número de clientes asociados a cada empleado como representante de soporte. La subconsulta cuenta el número de clientes en la tabla `Customer` donde el `SupportRepId` coincide con el `EmployeeId` del empleado en la tabla Employee. El resultado de esta subconsulta se asigna a la columna `N_Customers`.

```sql
SELECT EmployeeId, 
FirstName, 
LastName, 
(	SELECT COUNT(*) AS N_Customers
	FROM Customer
	WHERE Customer.SupportRepId = Employee.EmployeeId
) AS N_Customers
FROM Employee;
```

Esta consulta también selecciona el `EmployeeId`, `FirstName`, y `LastName` de la tabla `Employee`. Luego, realiza `LEFT JOIN` con una subconsulta que cuenta el número de clientes para cada empleado en la tabla `Customer`, agrupados por `SupportRepId`. La subconsulta devuelve dos columnas: `SupportRepId` y `N_Customers`. La unión se realiza en la columna `EmployeeId` de la tabla `Employee` y la columna `SupportRepId` de la subconsulta. Esto crea una tabla resultante que muestra el `EmployeeId`, `FirstName`, `LastName`, y `N_Customers` para cada empleado. Si un empleado no tiene clientes asignados, la columna `N_Customers` mostrará NULL.

```sql
SELECT EmployeeId,
FirstName,LastName, 
N_Customers
FROM Employee
LEFT JOIN (
	SELECT SupportRepId, COUNT(*) AS N_Customers
	FROM Customer
	GROUP BY SupportRepId
) AS Empleado_NCustomers
ON Employee.EmployeeId = Empleado_NCustomers.SupportRepId;

```

### Consulta 12
Ventas totales de cada empleado.

Esta consulta selecciona el `EmployeeId`, `FirstName`, y `LastName` de cada empleado. Luego, utiliza una subconsulta correlacionada para calcular las ventas totales de cada empleado. La subconsulta suma los totales de las facturas de los clientes asociados a cada empleado. Esto se logra al unir las tablas `Invoice` y `Customer` mediante una cláusula JOIN, relaciona los IDs de cliente entre las tablas y se filtran por el ID de empleado actual. El resultado se asigna como `TotalVentas`. Finalmente, la consulta ordena los resultados en orden descendente según el total de ventas de cada empleado.

```sql
SELECT 
e.EmployeeId,
e.FirstName,
e.LastName,
(	SELECT SUM(i.Total)
	FROM Invoice i
	JOIN Customer c ON i.CustomerId = c.CustomerId
	WHERE c.SupportRepId = e.EmployeeId
) AS TotalVentas
FROM Employee e
ORDER BY TotalVentas DESC;
```

### Consulta 13
Álbumes junto al número de canciones en cada uno.

Esta consulta selecciona el ID del álbum y el título del álbum de la tabla `Album`. La subconsulta cuenta el número de filas en la tabla `Track` donde el campo `AlbumId` coincide con el ID del álbum actual en la consulta principal. La subconsulta se ejecuta para cada fila de la tabla `Album`, lo que nos da el número de canciones en cada álbum.

```sql
SELECT 
AlbumId,
Title,
(	  SELECT COUNT(*) 
    FROM Track 
    WHERE Track.AlbumId = Album.AlbumId
) AS NumeroCanciones
FROM Album;


```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

En esta consulta seleccionamos la tabla `Artist` y a tráves de la subconsulta buscamos en la tabla `Album` seleccionamos el `Title` donde el `ArtistId` de álbum coincide con el `ArtistId` de la fila actual en la tabla `Artist`. Luego ordenamos los resultados por `AlbumId` y limitamos el resulato a 1 `LIMIT 1` para seleccionar el más reciente.

```sql
SELECT 
Artist.Name AS Artista,
(	  SELECT Album.Title
	  FROM Album
	  WHERE Album.ArtistId = Artist.ArtistId
	  ORDER BY Album.AlbumId DESC
	  LIMIT 1
) AS AlbumReciente
FROM Artist;
```


## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/


<!--
HAVING ??
GROUP BY ??
-->
