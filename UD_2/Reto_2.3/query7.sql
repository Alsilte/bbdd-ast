SELECT * 
FROM Album
WHERE AlbumId IN (
	SELECT AlbumId AS T_dur
	FROM Track
	GROUP BY AlbumId
	HAVING SUM(Milliseconds) > (
		SELECT AVG(T_dur) 
        FROM(
			SELECT AlbumId,
            SUM(Milliseconds) AS T_dur
			FROM Track
			GROUP BY AlbumId
			) 
		AS Album_TDur));