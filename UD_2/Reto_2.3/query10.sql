SELECT 
c.CustomerId,
c.FirstName,
c.LastName AS CustomerName,
    (SELECT SUM(i.Total)
     FROM Invoice i
     WHERE i.CustomerId = c.CustomerId
    ) AS TotalGastado
FROM 
    Customer c;