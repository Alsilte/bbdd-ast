SELECT *
FROM Track t
JOIN (
	SELECT g.GenreId
    FROM Genre g
    ORDER BY(
		SELECT COUNT(*) 
        FROM Track
        WHERE GenreId = g.GenreId
        ) DESC
	LIMIT 1 
    ) AS sub
ON t.GenreId = sub.GenreId;


       
