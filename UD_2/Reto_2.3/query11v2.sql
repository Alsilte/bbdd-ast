SELECT EmployeeId,FirstName,LastName, N_Customers
FROM Employee
LEFT JOIN (
	SELECT SupportRepId, COUNT(*) AS N_Customers
	FROM Customer
	GROUP BY SupportRepId
) AS Empleado_NCustomers
ON Employee.EmployeeId = Empleado_NCustomers.SupportRepId;