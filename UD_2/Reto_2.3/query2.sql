SELECT * 
FROM Invoice
WHERE CustomerId = 
		(SELECT CustomerId 
        FROM Customer 
        WHERE Email = "emma_jones@hotmail.com")
ORDER BY InvoiceId DESC
LIMIT 5;