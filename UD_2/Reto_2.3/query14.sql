SELECT 
Artist.Name AS Artista,
(	SELECT Album.Title
	FROM Album
	WHERE Album.ArtistId = Artist.ArtistId
	ORDER BY Album.AlbumId DESC
	LIMIT 1
) AS AlbumReciente
FROM Artist;
