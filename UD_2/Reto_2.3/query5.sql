SELECT a.AlbumId, 
a.Title, 
ar.Name 
FROM Album a
JOIN Artist ar 
ON a.ArtistId = ar.ArtistId
WHERE a.AlbumId IN (
	SELECT AlbumId FROM Track
	GROUP BY AlbumId
	HAVING COUNT(*) > 15)
ORDER BY a.AlbumId;