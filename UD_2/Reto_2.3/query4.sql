SELECT CustomerId,
FirstName AS Nombre,
LastName AS Apellido 
FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId 
    FROM Invoice
	WHERE Total > 20);