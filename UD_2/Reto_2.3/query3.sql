SELECT * 
FROM Playlist
WHERE PlaylistId IN (
	SELECT PlaylistId 
    FROM PlaylistTrack
	WHERE TrackId IN (
		SELECT TrackId 
        FROM Track
		WHERE GenreId = (
			SELECT GenreId 
            FROM Genre
			WHERE Name = "Reggae"
            )
		)
	);