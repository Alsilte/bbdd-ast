SELECT 
e.EmployeeId,
e.FirstName,
e.LastName,
(	SELECT SUM(i.Total)
	FROM Invoice i
	JOIN Customer c ON i.CustomerId = c.CustomerId
	WHERE c.SupportRepId = e.EmployeeId
) AS TotalVentas
FROM Employee e
ORDER BY TotalVentas DESC;
