# Reto 3: Consultas básicas

Alejandro S Tejero.

En este reto trabajamos con la base de datos `videoclub`, que nos viene dada en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace al repo](https://gitlab.com/Alsilte/bbdd-ast.git)
* [Enlace al ejercicio](https://gitlab.com/la-senia-db-2024/db/)


## Query 1
Para obtener el nombre de la película y el género, realizamos un join sobre las tablas `PELICULA` y `GENERE` poniendole un ALIAS, y usamos la sentencia `ON` para indicar que valores se usarán para "unificar las tablas", en este caso son `p.CodiGenere` y  `g.CodiGenere`:


```sql   
SELECT titol, Descripcio
FROM PELICULA AS p
JOIN GENERE AS g 
ON p.CodiGenere = g.CodiGenere;
```


## Query 2
Para obtener todas las facturas de María, realizamos un join sobre las tablas `FACTURA` y `CLIENT` poniendole un ALIAS, y usamos la sentencia `ON` 
para indicar que valores se usarán para "unificar las tablas", en este caso son 
`f.dni` y  `f.dni`. Y donde `WHERE` el nombre sea igual `LIKE` a Maria:


```sql
SELECT c.*
FROM FACTURA AS f
JOIN CLIENT AS c
ON f.dni = c.dni
WHERE NOM LIKE 'Maria%';
```

## Query 3
Para obtener las películas junto a su actor principal, realizamos un join sobre las tablas `PELICULA` y `ACTOR` poniendole un ALIAS, y usamos la sentencia `ON` 
para indicar que valores se usarán para "unificar las tablas", en este caso son 
`p.CodiActor` y  `a.CodiActor`:

```sql
SELECT p.Titol, a.Nom 
FROM PELICULA AS p
JOIN ACTOR AS a
ON p.CodiActor = a.CodiActor;
```

## Query 4
Para obtener las películas junto a todos los actores, realizamos un join sobre 
las tablas `PELICULA` , `INTERPRETADA` y `ACTOR` poniendole un ALIAS, y usamos la sentencia `ON` para indicar que valores se usarán para "unificar las tablas", en este caso son `p.CodiPeli` y  `i.CodiPeli`, además de `a.CodiActor` y `i.Codiactor`:

```sql
SELECT p.Titol, a.Nom 
FROM PELICULA AS p
JOIN INTERPRETADA AS i
JOIN ACTOR AS a
ON p.CodiPeli = i.CodiPeli
AND a.CodiActor = i.CodiActor;
```

## Query 5

Para obtener el ID y nombres de las películas junto a los ID y nombres de sus segundas partes, seleccionamos dos veces `CodiPeli`, y realizamos un join sobre la tabla `PELICULA` dos veces pero con un ALIAS diferente, y usamos la sentencia `ON` para indicar que valores se usarán para "unificar las tablas", en este caso son `p.CodiPeli` y  `S.SegonaPart`:

```sql
SELECT 
p.CodiPeli AS "Primera parte",
p.Titol,
s.CodiPeli AS "Segunda parte",
s.Titol
FROM PELICULA AS p
JOIN PELICULA AS s
ON p.CodiPeli = s.SegonaPart;
```