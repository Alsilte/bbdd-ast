SELECT 
p.CodiPeli AS "Primera parte",
p.Titol,
s.CodiPeli AS "Segunda parte",
s.Titol
FROM PELICULA AS p
JOIN PELICULA AS s
ON p.CodiPeli = s.SegonaPart;
