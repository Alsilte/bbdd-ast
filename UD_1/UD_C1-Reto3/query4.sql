SELECT p.Titol, a.Nom 
FROM PELICULA AS p
JOIN INTERPRETADA AS i
JOIN ACTOR AS a
ON p.CodiPeli = i.CodiPeli
AND a.CodiActor = i.CodiActor;
