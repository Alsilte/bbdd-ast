# Reto 1: Consultas básicas

Alejandro S Tejero.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados. Completado también con las observaciones del profersor Cristian en clase.

* [Enlace al repo](https://gitlab.com/Alsilte/bbdd-ast.git)
* [Enlace al ejercicio](https://gitlab.com/la-senia-db-2024/db/)


## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Podemos mostrar el alias de cada columna de diferentes formas. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;
SELECT 
    HOSPITAL_COD AS Código, 
    NOM AS 'Nombre', 
    TELEFON "teléfono"
FROM HOSPITAL;
```


## Query 2
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes que tengan una letra A en la segunda posición del nombre, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL` donde su `NOM` contenga con `LIKE` cualquier primer carácter y el segundo sea un a `_a%`. Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
USE sanitat;
SELECT 
    HOSPITAL_COD AS 'Número', 
    NOM AS 'Nombre', 
    TELEFON 
FROM HOSPITAL
WHERE NOM LIKE '_a%';
```

Otra forma de consultar sería con la función SUBSTRING().
```sql
USE sanitat;
SELECT HOSPITAL_COD AS 'Número', NOM AS 'Nombre', TELEFON FROM HOSPITAL
WHERE SUBSTRING(NOM, 2,1) = 'a';
```

## Query 3
Para seleccionar el código hospital, código sala, número empleado y apellido de todos los trabajadores existentes, seleccionaremos estos atributos, que se corresponden con las columnas `HOSPITAL_COD`, `SALA_COD`, `COGNOM`, y `EMPLEAT_NO`, respectivamente, de la tabla `PLANTILLA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;
SELECT hospital_cod AS 'Código hospital', 
    sala_cod AS 'Código sala', 
    empleat_no AS 'Numero empleado', 
    cognom AS 'Apellido'
FROM PLANTILLA;
```

## Query 4
Para seleccionar el código hospital, código sala, número empleado y apellido de todos los trabajadores existentes, seleccionaremos estos atributos, que se 
corresponden con las columnas `HOSPITAL_COD`, `SALA_COD`, `COGNOM`, y `EMPLEAT_NO`, respectivamente, de la tabla `PLANTILLA` y para obtener el turno contrario al de noche usamos un `NOT LIKE` de `N`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;
SELECT hospital_cod AS 'Código hospital', 
    sala_cod AS 'Código sala', 
    empleat_no AS 'Numero empleado', 
    cognom AS 'Apellido'
FROM PLANTILLA
WHERE torn NOT LIKE 'N';
```

Otras maneras de consultar que no es igual, sería de las siguientes formas: 

```sql
WHERE TORN != "N";
WHERE TORN <> "N";
WHERE TORN IN ("M", "T");
WHERE TORN = "M" OR TORN = "T";
WHERE NOT TORN IN ("N");
```


## Query 5
Para seleccionar todos los enfermos, selecionamos todos los atributos con el asterisco de la tabla `MALALT` y con un `WHERE YEAR` le decimos que sea igual al año correspondiente. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat; 
SELECT * FROM MALALT
WHERE YEAR(DATA_NAIX) = 1960;
```
Otras maneras de consultar fechas, sería de las siguientes formas: 

```sql
SELECT *

    month(DATA_NAIX),
    monthname(DATA_NAIX),
    day(DATA_NAIX),
    dayname(DATA_NAIX)

FROM MALALT;
```

## Query 6
Para seleccionar todos los enfermos, selecionamos todos los atributos con el asterisco de la tabla `MALALT` y con un `WHERE YEAR` le decimos que sea igual o mayor al año correspondiente. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat; 
SELECT * FROM MALALT
WHERE YEAR(DATA_NAIX) >= 1960;

--WHERE DATA_NAIX > DATE ("1959-12-12");
--WHERE DATA_NAIX BEETWEN DATE ("1960-01-01") AND current_date();
```