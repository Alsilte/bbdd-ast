# Reto 1: Consultas básicas

Alejandro S Tejero.

En este reto trabajamos con la base de datos `empresa` y `videoclub`, que nos viene dada en el fichero `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace al repo](https://gitlab.com/Alsilte/bbdd-ast.git)
* [Enlace al ejercicio](https://gitlab.com/la-senia-db-2024/db/)


## Query 1
Para seleccionar el código y descripción de los productos que comercializa la empresa, seleccionaremos estos dos atributos, que se corresponden con las columnas `PROD_NUM`Y `DESCRIPCIO` respectivamente, de la tabla `PRODUCTE`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE empresa;
SELECT 
    PROD_NUM AS 'Código', 
    DESCRIPCIO AS 'Nombre',    
FROM PRODUCTE;
```


## Query 2
Para seleccionar el código y descripción de los productos que comercializa la empresa, seleccionaremos estos dos atributos, que se corresponden con las columnas `PROD_NUM`Y `DESCRIPCIO` respectivamente, de la tabla `PRODUCTE`. Donde `DESCRIPCIO` tenga TENNIS .Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
SELECT 
PROD_NUM AS "Código",
DESCRIPCIO AS "Descripción"
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";
```

## Query 3
Para seleccionar el código, nombre, área y teléfono de los clientes de la empresa, seleccionaremos estos cuatro atributos, que se corresponden con las columnas `CLIENT_COD` , `NOM` , `AREA` y `TELEFON` respectivamente, de la tabla `CLIENT`.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT	
client_cod AS Código,
nom AS Nombre,
area AS Area,
telefon AS Telefono
FROM CLIENT;
```

## Query 4
Para seleccionar el código, nombre, área y teléfono de los clientes de la empresa, seleccionaremos estos cuatro atributos, que se corresponden con las columnas `CLIENT_COD` , `NOM` , `AREA` y `TELEFON` respectivamente, de la tabla `CLIENT`. Donde el `AREA` no se 636. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT	
client_cod AS Código,
nom AS Nombre,
ciutat AS Ciudad
FROM CLIENT
WHERE AREA NOT LIKE "636";
```

## Query 5
Para seleccionar el código, fechas de orden y de envío de las órdenes de compra, seleccionaremos estos dos atributos, que se corresponden con las columnas `COM_NUM`, `DATA_TRAMESA` y `COM_DATA` respectivamente, de la tabla `COMANDA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
COM_NUM AS Código,
COM_DATA AS "Fechas de orden",
DATA_TRAMESA AS "Fechas de envío"
FROM COMANDA;
```

## Query 6
Para seleccionar los nombres y teléfonos de los clientes, seleccionaremos los atributos que corresponden a  las columnas `NOM` y `TELEFON` respectivamente de la tabbla `CLIENT`.Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
NOM AS Nombres,
TELEFON AS Teléfono
FROM CLIENT;
```

## Query 7
Para seleccionar las fechas e importes de las facturas, seleccionaremos los atributos que corresponden a  las columnas `DATA` y `IMPORT` respectivamente de la tabla `FACTURA`. Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
SELECT 
DATA AS Fechas,
IMPORT AS Importe
FROM FACTURA;
```

## Query 8
Para seleccionar la descripción de los productos, seleccionaremos el atributo que corresponde a la columna `DESCRIPCIO`, de la tabla `DETALLEFACTURA`. Donde el código de la factura sea igual a 3. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT 
Descripcio AS Descripción
FROM DETALLFACTURA
WHERE CodiFactura LIKE "3";
```

## Query 9
Para listar las facturas, seleccionaremos todos los atributos con `*` de la tabla `FACTURA`, ordenado por el importe de forma descendente. Lo llevaremos a cabo con la siguiente sentencia SQL:
ordenado por el importe de forma descendente. Lo llevaremos a
```sql
SELECT 
*
FROM FACTURA
ORDER BY IMPORT DESC;
```

## Query 10
Para listar los actores, seleccionaremos todos los atributos con `*` de la tabla `ACTOR`, cuyo nombre contenga como primer carácter una x y un ásterisco para obtener cualquier cosa detrás de el. Lo llevaremos a cabo con la siguiente sentencia SQL:


```sql
SELECT * 
FROM ACTOR
WHERE NOM LIKE "x%"
```