SELECT COUNT(rf.idTipoReaccion) AS "Nº me divierte"
FROM tiposReaccion AS tr
JOIN reaccionesFotos AS rf
ON tr.idTipoReaccion = rf.idTipoReaccion
JOIN fotos AS f
ON rf.idFoto = f.idFoto
WHERE f.idFoto = 12
AND f.idUsuario = 45
AND rf.idTipoReaccion = 3;