SELECT rf.idFoto, f.descripcion
FROM tiposReaccion AS tr
JOIN reaccionesFotos rf
ON tr.idTipoReaccion = rf.idTipoReaccion
JOIN fotos AS f
ON rf.idFoto = f.idFoto
WHERE tr.idTipoReaccion = 4
AND rf.idUsuario = 11;