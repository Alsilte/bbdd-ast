# Consultas Instagram: Consultas básicas

Alejandro S Tejero.

En este reto trabajamos con la base de datos `instagram_low_cost`, que nos viene dada en el fichero `instagramLowCost2.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

* [Enlace al repo](https://gitlab.com/Alsilte/bbdd-ast.git)


## Query 1
Para obtener las fotos del usuario 36, seleccionamos la columna `descripcion` de la tabla `fotos`. Y a través del `WHERE` le decimos que solo queremos las del usuario 36.

```sql   
SELECT descripcion AS Descripción
FROM fotos 
WHERE idUsuario = 36;
```

## Query 2
Para obtener las fotos del usuario con ID 36 tomadas en enero del 2023, a partir de la consulta anterior ampliamos, y con las funciones `YEAR` y `MONTH` seleccionamos la fecha de las fotos que quermos obtener. En este caso puse 2024 porque en 2023 no habían.

```sql
SELECT descripcion AS Descripción
FROM fotos 
WHERE idUsuario = 36
AND YEAR(fechaCreacion) = 2024
AND MONTH (fechaCreacion) = 01;
```

## Query 3
Para obtener los comentarios del usuario 36 sobre la foto 12 del usuario 11, unimos las tablas con un `JOIN` las tablas `comentarios`,`comentariosFotos`,`fotos` y usamos la sentencia `ON` para indicar que valores se usarán para unificar las tablas, en este caso a través de `idComentario` y `idFoto`. Por último con el `WHERE` seleccionamos las características que se piden.

```sql
SELECT c.comentario 
FROM comentarios AS c
JOIN comentariosFotos AS cf
ON c.idComentario = cf.idComentario
JOIN fotos AS f
ON cf.idFoto = f.idFoto
WHERE c.idUsuario = 36
AND cf.idFoto = 12
AND f.idUsuario = 11;
```

## Query 4
Para obtener las fotos que han sorprendido al usuario 25, unimos las tablas `tiposReaccion`, `reaccionesFotos` y fotos con un `JOIN` y usamos la sentencia `ON` para indicar que valores se usarán para unificar las tablas, en este caso a través de `idTipoReaccion` y `idFoto`. Por último con el `WHERE` seleccionamos las características que se piden.

```sql
SELECT rf.idFoto, f.descripcion
FROM tiposReaccion AS tr
JOIN reaccionesFotos rf
ON tr.idTipoReaccion = rf.idTipoReaccion
JOIN fotos AS f
ON rf.idFoto = f.idFoto
WHERE tr.idTipoReaccion = 4
AND rf.idUsuario = 11;
```

## Query 5

Para obtener los administradores que han dado más de 2 “Me gusta”, unimos las tablas `tiposReaccion`, `reaccionesComentarios`, `reaccionesFotos`, usuarios con un `JOIN` y usamos la sentencia `ON` para indicar que valores se usarán para unificar las tablas, en este caso a través de `idTipoReaccion`, `idUsuario` y `idFoto`. Por último con el `WHERE` seleccionamos las características que se piden.

```sql
SELECT u.nombre
FROM tiposReaccion AS tr
JOIN  reaccionesComentarios AS rc
ON tr.idTipoReaccion = rc.idTipoReaccion
JOIN reaccionesFotos AS rf
ON tr.idTipoReaccion = rf.idTipoReaccion
JOIN usuarios AS u
ON u.idUsuario = rf.idUsuario
AND u.idUsuario = rc.idUsuario
WHERE u.idRol = 3
AND tr.idTipoReaccion = 1;

```
## Query 6
Para obtener el número de “Me divierte” de la foto número 12 del usuario 45, unimos las tablas `tiposReaccion`, `reaccionesFotos`, `fotos` con un `JOIN` y usamos la sentencia `ON` para indicar que valores se usarán para unificar las tablas, en este caso a través de `idTipoReaccion` y `idFoto`. Por último con el `WHERE` seleccionamos las características que se piden.

```sql
SELECT COUNT(rf.idTipoReaccion) AS "Nº me divierte"
FROM tiposReaccion AS tr
JOIN reaccionesFotos AS rf
ON tr.idTipoReaccion = rf.idTipoReaccion
JOIN fotos AS f
ON rf.idFoto = f.idFoto
WHERE f.idFoto = 12
AND f.idUsuario = 45
AND rf.idTipoReaccion = 3;
```
## Query 7

Para obtener el número de fotos tomadas en la playa (en base al título), utilizamos un `COUNT()` de la descripción para contar las fotos, de la tabla `fotos` de cada foto, donde la descripción de cada foto contenga la palabra playa a través de un `LIKE` 

```sql
SELECT COUNT(descripcion) "Nº de fotos en la playa"
FROM fotos
WHERE descripcion LIKE "%playa%";
```
