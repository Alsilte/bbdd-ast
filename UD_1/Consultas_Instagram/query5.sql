SELECT *
FROM roles
JOIN usuarios
ON usuarios.idRol = roles.idRol
JOIN reaccionesFotos
ON usuarios.idUsuario = reaccionesFotos.idUsuario
JOIN tiposReaccion
ON tiposReaccion.idTipoReaccion = reaccionesFotos.idTipoReaccion
WHERE roles.descripcion = "Usuario" AND tiposReaccion.descripcion = "Me gusta"

UNION

SELECT *
FROM roles
JOIN usuarios
ON usuarios.idRol = roles.idRol
JOIN reaccionesComentarios
ON usuarios.idUsuario = reaccionesComentarios.idUsuario
JOIN tiposReaccion
ON tiposReaccion.idTipoReaccion = reaccionesComentarios.idTipoReaccion
WHERE roles.descripcion = "Usuario" AND tiposReaccion.descripcion = "Me gusta";

