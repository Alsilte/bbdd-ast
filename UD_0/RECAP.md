# Unidad C0: Recapitulación

Alejandro S Tejero.

El siguiente documentoes una introducción a las bases de datos para un lector inexperto en el tema, para que pueda comprender su funcionamiento.

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Las bases de datos se encargan de almacenar datos y donde podemos relacionar-los entre sí, estos datos pueden ser de diferentes tipos. 

Tratan de resolver el acceso de los datos de una forma más sencilla. 


## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

Es un software que permite acceder a los datos de una BBDD. Permite modificar, actualizar, eliminiar e insertar datos.

Acceso eficiente a los datos : Los SGBD están diseñados para ofrecer un acceso rápido y eficiente a los datos. Utilizan índices y optimizaciones internas para acelerar las consultas y reducir el tiempo de respuesta.

La integridad: Los SGBD garantizan la integridad de los datos mediante la aplicación de reglas y restricciones. Esto asegura que los datos sean consistentes y estén libres de errores o duplicaciones.

Seguridad: Los SGBD ofrecen mecanismos de seguridad robustos para proteger la información confidencial. Establecen niveles de acceso y autenticación para garantizar que solo los usuarios autorizados puedan acceder a determinados datos.

La disponibilidad de los datos : capacidad de un servicio, de unos datos o de un sistema, a ser accesible y utilizable por los usuarios (o procesos) autorizados cuando estos lo requieran.

### Ejemplos de sistemas de gestión de bases de datos

¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

 El software libre es el software que respeta la libertad de los usuarios y la comunidad. A grandes rasgos, significa que los usuarios tienen la libertad de ejecutar, copiar, distribuir, estudiar, modificar y mejorar el software. Es decir, el «software libre» es una cuestión de libertad, no de precio. [[1](#referencias)].

* Oracle DB (Cliente-servidor)
* IMB Db2 (Cliente-servidor)
* SQLite (Software libre) (BBDD embebida)
* MariaDB (Software libre) (Cliente-servidor )
* SQL Server (Cliente-servidor)
* PostgreSQL (Software libre) (Cliente-servidore o embebida)
* mySQL (Software libre) (Cliente-servidor)
* Mongo DB (Cliente-servidor)

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

El modelo cliente servidor permite desde diferentes ordenadores conectarme a la misma base de datos, pudiendo tener la base de datos en un lugar y poder acceder a ella desde diferentes partes.

* __Cliente__: Máquna o software que se encarga de pedir peticiones al sevidor.
* __Servidor__: Ordenador o software que ofrece un servicio.
* __Red__: Comunica el cliente con el servidor.
* __Puerto de escucha__: Un puerto es la dirección de un programa cuando los datos viajan de una computadora a otra a través de una red. interfaz a través de la cual se pueden enviar y recibir los diferentes tipos de datos. 
* __Petición__: Mensaje que demanda el cliente al sevidor.
* __Respuesta__: Mensaje que devuelve el servidor al cliente ante una petición.

## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?

Es el lenguaje estándar desde los años 80 que nos permite hacer consultas a la base de datos entre otras cosas.
Un lenguaje de consulta es un lenguaje informático usado para hacer consultas en bases de datos y sistemas de información. 

Los lenguajes de 4ª generación o 4GL especifican qué resultados se quieren obtener y no cómo deben obtenerse. No es necesario definir los pasos a seguir en un programa para realizar una tarea determinada, sino una serie de parámetros que serán utilizados para generar un programa.

Los lenguajes declarativos son lenguajes de programación lógica. Los lenguajes de programación lógica son considerados lenguajes declarativos porque, así como los funcionales, no requieren especificar paso a paso una estructura ordenada de instrucciones al ordenador para obtener un resultado.


### Instrucciones de SQL
 
#### DDL
Definición de datos (DDL): Permite crear, modificar y eliminar estructuras de la base de datos (tablas, índices, etc.).

- CREATE 
- ALTER
- DROP 


#### DML
Manipulación de datos (DML): Permite insertar, modificar y eliminar datos en las tablas.Manipulación de datos (DML): Permite insertar, modificar y eliminar datos en las tablas.

- UPDATE
- SELECT
- INSERT

#### DCL
Control de datos (DCL): Permite gestionar permisos de acceso y usuarios de la base de datos.

- GRANT
- REVOKE

#### TCL
Lenguaje de Control de Transacciones (TCL):  es un subconjunto de SQL que se utiliza para gestionar el comportamiento de las transacciones dentro de una base de datos.

- COMMIT
- ROLLBACK

[[2](##referencias)].

## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

Una BBDD relacional es aquella que permite interactuar entre las diferentes tablas y poder obtener un resultado en las consultas más específico. 

* __Relación (tabla)__: Estructuras que almacenan los datos. 
* __Atributo/Campo (columna)__:  Campos que contienen los diferentes tipos de datos que se almacenan en una tabla. 
* __Registro/Tupla (fila)__: Datos individuales que se almacenan en una tabla.

## Referencias

[1] https://www.ucm.es/oficina-de-software-libre

[2] https://es.wikipedia.org/wiki/SQL#Caracter%C3%ADsticas_generales_de_SQL