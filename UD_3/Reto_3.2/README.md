# Unidad C3: Conexión, Definición de datos (DDL) y Control de acceso (DCL)

Alejandro S Tejero.

El siguiente documento trata sobre el control de acceso (DDL)  y  las opciones de control de acceso que ofrece MySQL explicado desde un conocimiento básico.

* [Enlace al repositorio](https://gitlab.com/Alsilte/bbdd-ast.git)

## Control de acceso
###  Registrar nuevos usuarios, modificarlos y eliminarlos.

- `CREATE USER 'alejandro'@'localhost' IDENTIFIED BY 'alejandro';`. Creación de un usuario nuevo.

- `RENAME USER 'alejandro'@'localhost' TO 'alejandro'@'%';`. Modificar datos de un usuario. 

- `DROP USER 'alejandro'@'%';`. Eliminación de usuario.

- `mysql -u root -p -e "ALTER USER 'alejandro'@'%' IDENTIFIED BY 'nueva_contraseña'"`. Cambiar la contraseña del usuario.


### Autenticación de los usuarios y qué opciones de autenticación nos ofrece el SGBD.

MySQL ofrece varias opciones de autenticación. Dejo como ejemplo los comandos básicos para crear usuarios con los diferentes métodos de autenticación disponibles en MySQL. Cada método puede requerir configuraciones adicionales dependiendo del entorno y los requisitos específicos de seguridad por ello todos no funcionan solo con este comando.

#### Autenticación Nativa de MySQL (mysql_native_password)

 Utiliza un algoritmo de hash para las contraseñas basado en SHA-1.

```sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'mysql_native_password' BY 'password';
```

* [Documentación oficial: Native Pluggable Authentication](https://dev.mysql.com/doc/refman/8.0/en/native-pluggable-authentication.html)

#### Caching SHA-2 Pluggable Authentication

Utiliza un algoritmo de hash SHA-256.

```sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'caching_sha2_password' BY 'password';
```

* [Documentación oficial: Caching SHA-2 Pluggable Authentication](https://dev.mysql.com/doc/refman/8.0/en/caching-sha2-pluggable-authentication.html)

#### Autenticación Basada en Plugin PAM

Permite el uso de Pluggable Authentication Modules (PAM) para autenticación.

```sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'pam' BY 'password';
```

* [Documentación oficial: PAM](https://dev.mysql.com/doc/refman/8.0/en/pam-pluggable-authentication.html)

#### Autenticación basada en LDAP

Permite a MySQL autenticar usuarios contra un servidor LDAP.

```sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'auth_ldap' AS 'cn=usuario,dc=example,dc=com';
```

* [Documentación oficial: LDAP](https://dev.mysql.com/doc/refman/8.0/en/ldap-pluggable-authentication.html)

#### Autenticación basada en Kerberos

Utiliza el protocolo Kerberos para la autenticación.

```sql
CREATE USER 'usuario'@'host' IDENTIFIED WITH 'auth_gssapi';
```
* [Documentación oficial: Kerberos](https://dev.mysql.com/doc/refman/8.0/en/kerberos-pluggable-authentication.html)

#### Autenticación a través de SSL/TLS

Además de verificar la identidad del servidor, SSL/TLS puede requerir certificados de cliente para autenticar usuarios.

```sql
CREATE USER 'usuario'@'host' REQUIRE SSL;
```

* [Documentación oficial: SSL/TLS](https://dev.mysql.com/doc/refman/8.0/en/creating-ssl-rsa-files.html)

[[1](#referencias)].

### Mostrar los usuarios existentes y sus permisos.

- `SELECT user, host FROM mysql.user;`  Podemos ver los usuario de mysql.

- `SHOW GRANTS  FOR 'alejandro'@'%';` ver privilegios del usuario.

###  Permisos que puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.

- `GRANT ALL PRIVILEGES ON * . * TO 'nuevo_usuario'@'localhost';` . Este comando da todos los privilegios al usuario.

- `FLUSH PRIVILEGES;`. Para que los cambios tengan efecto inmediatamente.

El nivel de granularidad es como de específicos son los permisos para un usuario, de tener permisos para todas las bases de datos a tener permisos en una sola tabla de una base de datos o solo lectura de una base de datos.

### Posibilidad de agrupar los usuarios (grupos/roles/...).

- `CREATE ROLE 'jefe', 'usuario', 'desarrollador';`. Creamos tres roles diferentes.

- `GRANT ALL ON Chinook.* TO 'jefe';`. El jefe tiene todos los privilegios.

- `GRANT SELECT ON Chinook.* TO 'usuario';`. El usuario solo tiene privilegios para consultar datos de la tabla.

- `GRANT INSERT, UPDATE, DELETE ON Chinook.* TO 'desarrollador';`. El desarrollador solo tiene privilegios para insertar, actualizar y borrar.

- `GRANT 'jefe' TO 'alejandro'@'%';`. Añade el rol jefe a alejandro DESDE ROOT.

- `SET ROLE 'usuario_raso';` Añade un rol a un usuario. DESDE EL USUARIO PODREMOS AÑADIR-NOS AL ROL QUE NOS HAYAN DADO DESDE ROOT

- `SELECT CURRENT_ROLE();` Muestra el rol del usuario.

- `REVOKE ALL PRIVILEGES ON * . * FROM 'usuario'@'localhost';`

[[2](#referencias)].

### Porque root puede hacer lo que quiera 

- `SELECT Host, User, Grant_priv FROM user;` Verifica que permisos posee cada usuario existente en tu gestor de bases de dato.

```sql
+-----------+------------------+------------+
| Host      | User             | Grant_priv |
+-----------+------------------+------------+
| localhost | demo             | N          |
| localhost | mysql.infoschema | N          |
| localhost | mysql.session    | N          |
| localhost | mysql.sys        | N          |
| localhost | root             | Y          |
+-----------+------------------+------------+
```

Solo root puede dar permisos a otros usuarios como vemos en la tabla anterior.

`GRANT ALL PRIVILEGES ON *.* TO 'demo'@'tuhost' WITH GRANT OPTION;`

La clausula opcional WITH es usada para habilitar al usuario a conceder permisos a otros usuarios. La opción WITH GRANT OPTION le brinda al usuario la habilidad de dar a otros usuarios cualquier privilegio que tenga el usuario en el nivel de privilegio especificado.

[[3](#referencias)].


## Referencias

[1] https://dev.mysql.com/doc/search/?d=201&p=1&q=Authentication

[2] https://dev.mysql.com/doc/refman/8.0/en/privileges-provided.html

[3] https://es.stackoverflow.com/questions/297155/como-dar-privilegios-grant-al-usuario-root-mysql-8-para-acceder-desde-otro-host
