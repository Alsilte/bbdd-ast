# Unidad C3: Conexión, Definición de datos (DDL) y Control de acceso (DCL)

Alejandro S Tejero.

El siguiente documento trata sobre la conexión a un BBDD, la definición de los datos (DDL) y el control de acceso (DDL), explicado desde un conocimiento básico.

* [Enlace al repo](https://gitlab.com/Alsilte/bbdd-ast.git)

## Conexión a la BBDD
### Conexión a la BBDD desde una linea de comandos

Podemos acceder a las BBDD de mysql desde la linea de comando sin usar el interfaz gráfico de Workbench, a través de los siguientes comandos:

- `mysql -u root -p -h 127.0.0.1 -P 33006`

- `mysql`: Este es el comando principal que se utiliza para iniciar la interfaz de línea de comandos de MySQL. 

- `u root`: Esto especifica el nombre de usuario con el que deseas conectarte al servidor

- `p`: Esta opción indica que se debe solicitar la contraseña después de ingresar el nombre de usuario.

- `h 127.0.0.1`: Representa la dirección IP del servidor MySQL al que deseas conectarte. 
C
- `P 33006`: Esta opción especifica el puerto en el que el servidor MySQL está escuchando.

### Consultas básicas desde una linea de comandos:

En la propia linea de comandos una vez hemos accedido podemos hacer consultas como hemos visto en el interfaz gráfico de Workbench. A continuación podemos ver las principales consultas para ver que bases de datos tenemos, que tablas contienen entre otros, que nos va a ser útil para la linea de comandos.

`SHOW DATABASES;` muestra una lista de todas las bases de datos disponibles.

`SHOW SCHEMAS`  muestra lo mismo que el comando anterior.

`SHOW TABLES FROM Chinook;` enumera las tablas en la base de datos “Chinook”.

`USE Chinook;` cambia la base de datos actual a “Chinook”.

`SHOW TABLES;` muestra las tablas en la base de datos actual.

`SHOW COLUMNS FROM Employee;` proporciona información sobre las columnas de la tabla “Employee”.

`DESCRIBE Employee;` es equivalente a la consulta anterior.

`SELECT DATABASE()` muestra qen que BBDD estamos usando ahora mismo.

`SELECT * FROM mysql.users` muestra todos los usuarios de mysql.

También podemos realizar todas las consultas que hemos ido viendo durante el curso, como realizar UPDATES, DROP, INSERTS entre otros.

## Control de transacciones:

El control de transacciones es un concepto fundamental en las bases de datos que garantiza la integridad y consistencia de los datos durante las operaciones.

Un ejemplo sería el de un cajero automático, en el que un cliente intenta sacar dinero y cuando ha seleccionado la opción de retirar dinero se apaga el cajero, gracias a las transacciones no se actualizaría la BBDD ya que no ha terminado todo el proceso. Si el cajero no utilizara transacciones, el saldo del cliente se habría actualizado antes de entregar el efectivo. En este caso, si el cajero se apaga, el cliente perdería dinero porque el saldo se ha reducido, pero no recibió el efectivo.

`COMMIT;` confirma los cambios pendientes en la base de datos. Después de ejecutar COMMIT, los datos modificados se guardan permanentemente.

`ROLLBACK;` revierte los cambios no confirmados al estado anterior al último COMMIT.

`SET autocommit=0;` deshabilita la confirmación automática de cambios en la base de datos. Esto significa que los cambios no se aplicarán hasta que realices un COMMIT. Si lo ponemos en 1 hace el autocommit.

## Creación de BBDD, tablas y campos:

¿Qué tipos elegimos? ¿Qué campos necesitamos?
¿Qué restricciones ponemos?¿Por qué?

Hemos creado la BBDD con el siguiente comando:

```sql
CREATE SCHEMA Vuelos ;
```
Para crear las tablas de la BBDD utilizaremos el siguiente comando: 
```sql
CREATE TABLE Pasajeros (
  idPasajero int NOT NULL,
  Nombre varchar(45) NOT NULL,
  Documento varchar(45) NOT NULL,
  TipoDocumento varchar(45) NOT NULL,
  FechaNacimineto date NOT NULL,
  Nacionalidad varchar(45) NOT NULL,
);

CREATE TABLE Reservas (
  idReservas int NOT NULL,
  idPasajero int NOT NULL,
  idVuelo int NOT NULL,
  Asiento char(3) NOT NULL,
);

CREATE TABLE Vuelos (
  idVuelos int NOT NULL,
  NPlazas int DEFAULT NULL,
  Origen varchar(3) DEFAULT NULL,
  Destino varchar(3) DEFAULT NULL,
  Compañia varchar(45) DEFAULT NULL,
  FechaHora datetime DEFAULT NULL,
);
```

En las tablas anteriores hemos creado diferentes tipos de datos.

- `varchar` : para datos con caracteres 
- `int` : para datos numéricos
- `datetime` : para 

En cuanto a las restricciones tenemos:

- `NOT NULL` : para aquellos datos que queremos que siempre tengan un valor, ya que pueda ser esencial tener esa información
- `DEFAULT` : para que si el valor es null, por defecto tenga un valor que nosotros le decimos.

Para poder cambiar el nombre de la tabla utilizamos el siguiente comando:

```sql
ALTER TABLE Ryanair.Vuelos 
RENAME TO  Ryanair.Vuelo ;
```

Con el siguiente comando creamos la clave foránea para relacionar idVuelo con la tabla intermedia reservas. Ya que la relación entre la tabla Vuelos y Pasajeros es de N:M.

```sql
ALTER TABLE Reservas 
ADD CONSTRAINT fk_Reservas_2
  FOREIGN KEY (idVuelo)
  REFERENCES Vuelos (idVuelos);
```

Con el siguiente comando cambiamos el tipo de datos de una columna:

```sql
ALTER TABLE Reservas
CHANGE COLUMN idPasajero idPasajero INT NOT NULL,
CHANGE COLUMN idVuelo idVuelo INT NOT NULL;
```
Campos generados se utilizan para crear un dato único a partir de dos valores, ya que alguno de estos pueda coincidir entre diferentes valores, como que el documento sea igual para dos personas de diferente país pero nunca del mismo: 

```sql
ALTER TABLE Pasajeros 
CHANGE COLUMN NacionalidadDocumento NacionalidadDocumento VARCHAR(45) GENERATED ALWAYS AS (concat(Nacionalidad,Documento)) VIRTUAL ;
```

¿Porque necesita una relación e integridad esta BBDD?

Necesitamos tener integridad en la BBDD en este caso para que cuando se genere una reserva de un vuelo, ese vuelo exista y el pasajero también ya que sino se generaría un vuelo que no existe o una reserva de un pasajero que no exista.

